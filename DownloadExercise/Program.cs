﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using DownloadExercise.Model;
using Newtonsoft.Json;

namespace DownloadExercise
{
    public class Program
    {
        private static string RootUrl = "http://doanit.com/english4listen";
        
        private static string StoreFolderName = "Listen";

        private static string RootFolder = Path.Combine(@"D:\data", StoreFolderName);

        private static int DownloadedCount = 0;
        private static int ItemCount = 0;

        private static string DownloadString(string url)
        {
            using (var stream = new WebClient().OpenRead(url))
            {
                if (stream != null)
                {
                    return new StreamReader(stream).ReadToEnd();
                }
                throw new Exception("Error");
            }
        }

        private static string CheckUrl(string url)
        {
            try
            {
                var relativePath = url.Substring(RootUrl.Length + 1);

                var relativeFolder = relativePath.Replace("/", "\\");

                var filePath = Path.Combine(RootFolder, relativePath);

                var destinateFolder = Path.GetDirectoryName(filePath);
                
                relativePath = StoreFolderName + "/" + relativePath;
                if (File.Exists(filePath))
                {
                    return relativePath;
                }

                var data = new WebClient().DownloadData(url);
                if (!Directory.Exists(destinateFolder))
                {
                    Directory.CreateDirectory(destinateFolder);
                }

                File.WriteAllBytes(filePath, data);
                Console.WriteLine("Downloaded::: " + url + "\t" + data.Length + " bytes");
                return relativePath;
            }
            catch (WebException ex)
            {
                return default(string);
            }
            catch (Exception ex)
            {
                return default(string);
            }
        }

        private static List<ListenContent> Read(
            DataItem dataItem, 
            string menu, 
            string subMenu, 
            string path)
        {
            if (dataItem.DataItems != null && dataItem.DataItems.Any())
            {
                return dataItem.DataItems.Where(x=>!x.FileName.Contains("Copy"))
                    .SelectMany(x => Read(x,!string.IsNullOrEmpty(menu)?menu:x.Display, (!string.IsNullOrEmpty(menu) && string.IsNullOrEmpty(subMenu))? x.Display: subMenu, 
                    path + "/" + x.FileName)).ToList();
            }
            try
            {
                var contentJson = DownloadString($"{path}.json");
                var content = JsonConvert.DeserializeObject<ListenContent>(contentJson);

                content.Menu = menu;
                content.SubMenu = subMenu;
                content.Lesson = dataItem.Display;
                content.PhotoLink = CheckUrl($"{path}.jpg");
                content.Mp3Link = CheckUrl($"{path}.mp3");
                DownloadedCount++;

                Console.WriteLine($"Download {DownloadedCount}/{ItemCount}: {DownloadedCount/ItemCount*100} %");

                return new List<ListenContent>
                {
                    content
                };
            }
            catch
            {
                return new List<ListenContent>
                {
                    
                };
            }
        }

        private static int Calculate(DataItem dataItem)
        {
            if(dataItem.DataItems==null || dataItem.DataItems.Count == 0)
            {
                return 1;
            }

            return dataItem.DataItems.Sum(Calculate);
        }

        static void Main(string[] args)
        {
            var dataJson = DownloadString("http://doanit.com/english4listen/data.json");
            var dataItems = JsonConvert.DeserializeObject<DataItem>(dataJson);

            ItemCount = Calculate(dataItems);
            var tests = Read(dataItems, "", "", RootUrl);

            File.WriteAllText("data.json", JsonConvert.SerializeObject(tests, 
                new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                }));
        }
    }
}
