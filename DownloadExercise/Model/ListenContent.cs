﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DownloadExercise.Model
{
    [DataContract]
    public class ListenContent
    {
        [DataMember(Name = "menu")]
        public string Menu { get; set; }

        [DataMember(Name = "submenu")]
        public string SubMenu { get; set; }

        [DataMember(Name = "lesson")]
        public string Lesson { get; set; }

        [DataMember(Name = "script")]
        public string Script { get; set; }

        [DataMember(Name = "questions")]
        public List<Question> Questions { get; set; }

        [DataMember(Name = "mp3Link", EmitDefaultValue = true)]
        public string Mp3Link { get; set; }

        [DataMember(Name = "photo", EmitDefaultValue = true)]
        public string PhotoLink { get; set; }
    }
}
