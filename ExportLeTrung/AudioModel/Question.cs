﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ExportLeTrung.AudioModel
{
    [DataContract]
    public class Question
    {
        [DataMember(Name = "correctAnswer")]
        public long CorrectAnswer { get; set; }

        [DataMember(Name = "answers")]
        public List<string> Answers { get; set; }

        [DataMember(Name = "question")]
        public string OtherQuestion { get; set; }
    }
}
