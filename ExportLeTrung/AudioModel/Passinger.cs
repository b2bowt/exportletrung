﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ExportLeTrung.AudioModel
{
    [DataContract]
    public class Passinger
    {
        [DataMember(Name = "questions")]
        public List<Question> Questions { get; set; }

        [DataMember(Name = "paragraph")]
        public string Paragraph { get; set; }
    }
}
