﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ExportLeTrung.AudioModel
{
    public class IncompleteSentence
    {
        [DataMember(Name = "questions")]
        public List<Question> Questions { get; set; }
    }
}
