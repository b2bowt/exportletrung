﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ExportLeTrung.AudioModel
{
    [DataContract]
    public class Photograph : QuestionResponse
    {
        [DataMember(Name = "photo")]
        public string PhotoLink { get; set; }

        public override void SetUrl(string url)
        {
            base.SetUrl(url);
            PhotoLink = url + ".png";
        }
    }
}
