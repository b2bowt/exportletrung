﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace ExportLeTrung.AudioModel
{
    [DataContract]
    public class QuestionResponse : IOnline
    {
        [DataMember(Name = "questions")]
        public List<Question> Questions { get; set; }

        [DataMember(Name = "script")]
        public string Script { get; set; }

        [DataMember(Name = "mp3Link")]
        public string Mp3Link { get; set; }

        public virtual void SetUrl(string url)
        {
            Mp3Link = url + ".mp3";
        }
    }
}
