﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ExportLeTrung.AudioModel;
using Newtonsoft.Json;

namespace ExportLeTrung
{
    public class FileItemParser
    {
        private const string dataRoot = @"D:\Project\toeictestandroid\app\src\main\assets";

        public static List<T> ParseFileItem<T>(FileItem fileItem, string folderPath)
        {
            if (fileItem.Children == null || !fileItem.Children.Any())
            {
                var filePath = dataRoot + @"\" + folderPath + ".json";
                var fileContent = File.ReadAllText(filePath);
                var data = JsonConvert.DeserializeObject<T>(fileContent);

                if (data is IOnline iOnline)
                {
                    iOnline.SetUrl(folderPath.Replace(@"\", "/"));
                }

                return new List<T>() { data };
            }

            return fileItem.Children.SelectMany(x=>ParseFileItem<T>(x, folderPath+@"\" + x.FileName)).ToList();
        }

        public static void ParseFileItem<T>(string key, FileItem fileItem)
        {
            var child = fileItem.Children.FirstOrDefault(x => x.FileName == key);
            var data =  ParseFileItem<T>(child, key);

            File.WriteAllText(key + ".json", JsonConvert.SerializeObject(data));
        }


        public static void Export()
        {
            var fileItem = JsonConvert.DeserializeObject<FileItem>(File.ReadAllText(dataRoot + @"\data.json"));

            ParseFileItem<QuestionResponse>("question_responses", fileItem);
            ParseFileItem<ShortTalk>("short_talks", fileItem);
            ParseFileItem<Photograph>("photographs", fileItem);
            ParseFileItem<Passinger>("passinger", fileItem);
            ParseFileItem<IncompleteSentence>("Incomplete_sentences", fileItem);
            ParseFileItem<Conversation>("conversations", fileItem);
        }
    }
}
