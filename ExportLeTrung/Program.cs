﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExportLeTrung.AudioModel;
using Newtonsoft.Json;
using System.Net;

namespace ExportLeTrung
{
    class Program
    {
        private static string dataFolder = @"D:\data";

        private static string RootUrl = "http://doanit.com/english4listen";

        private static string StoreFolderName = "Listen";

        private static string RootFolder = Path.Combine(@"D:\data", StoreFolderName);

        private static string CheckUrl(string url)
        {
            try
            {
                var relativePath = url.Substring(RootUrl.Length + 1);

                var relativeFolder = relativePath.Replace("/", "\\");

                var filePath = Path.Combine(RootFolder, relativePath);

                var destinateFolder = Path.GetDirectoryName(filePath);

                relativePath = StoreFolderName + "/" + relativePath;
                
                if (File.Exists(filePath))
                {
                    return relativePath;
                }

                var data = new WebClient().DownloadData(url);

                if (!Directory.Exists(destinateFolder))
                {
                    Directory.CreateDirectory(destinateFolder);
                }

                
                File.WriteAllBytes(filePath, data);
                Console.WriteLine("Downloaded::: " + url + "\t" + data.Length + " bytes");
                return relativePath;
            }
            catch (WebException ex)
            {
                return default(string);
            }
            catch (Exception ex)
            {
                return default(string);
            }
        }

        private static List<T> Parse<T>(string fileName)
        {
            Console.WriteLine("Starting download : " + fileName);
            var filePath = Path.Combine(dataFolder, $"{fileName}.json");
            var fileContent = File.ReadAllText(filePath);
            var data = JsonConvert.DeserializeObject<List<T>>(fileContent);

            foreach (var item in data.OfType<QuestionResponse>())
            {
                item.Mp3Link = CheckUrl(RootUrl + "/" + item.Mp3Link);
            }

            foreach (var item in data.OfType<Photograph>())
            {
                item.PhotoLink = CheckUrl(RootUrl + "/" + item.PhotoLink);
            }

            Console.WriteLine("Downloaded done for " + fileName);

            return data;
        }

        static void Main(string[] args)
        {
            var shortTalks = Parse <ShortTalk>("short_talks");
            var questionResponsess = Parse<QuestionResponse>("question_responses");
            var photographs = Parse<Photograph>("photographs");
            var passingers = Parse<Passinger>("passinger");
            var incompleteSentences = Parse<IncompleteSentence>("Incomplete_sentences");
            var conversations = Parse<Conversation>("conversations");
        }
    }
}
