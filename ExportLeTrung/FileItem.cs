﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ExportLeTrung
{
    [DataContract]
    public class FileItem
    {
        [DataMember(Name = "display")]
        public string Display { get; set; }

        [DataMember(Name = "children")]
        public List<FileItem> Children { get; set; }

        [DataMember(Name = "fileName")]
        public string FileName { get; set; }
    }
}
